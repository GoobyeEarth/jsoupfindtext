import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;

import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.text.Document;

import org.jsoup.Jsoup;

public class Main {
	public static void main(String[] args) {
		
		
		sample();
	}

	private static void sample(){
		FindTextFrame frame = new FindTextFrame();
		frame.view();
		frame.listener();
		
	}
	
	
	
}
