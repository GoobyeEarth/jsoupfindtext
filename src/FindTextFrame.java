import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.Rectangle;
import java.awt.ScrollPane;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.NoRouteToHostException;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.jsoup.Jsoup;

import library.widget.JsoupControllerClass;

public class FindTextFrame{
	
	
	private JTextField urlText;
	private org.jsoup.nodes.Document htmlDocument;
	private JButton goButton;
	private GridBagConstraints mainGridConstraints;
	
	private JLabel urlLoaded;
	private JPanel mainGridBugPanel;
	private JFrame frame;
	private JButton searchButton;
	private JTextField contentsText;
	private JPanel scrollGridBugPanel;
	private GridBagConstraints scrollGridConstraints;
	
	private JTextArea htmlArea;
	private JButton htmlButton;
	
	public void view(){
		mainGridBugPanel = mainGridBugPanel();
		mainGridConstraints = new GridBagConstraints();
		mainGridConstraints.fill = GridBagConstraints.BOTH;
		
		JPanel htmlPanel = new JPanel();
		
		JScrollPane htmlScrollPane = setScrollPane(htmlPanel);
		mainGridConstraints.gridx = 0;
		mainGridConstraints.gridy = 0;
		mainGridConstraints.weightx = 1;
		mainGridConstraints.weighty = 0.2;
		mainGridConstraints.gridwidth = 1;
		
		htmlArea = new JTextArea();
		htmlPanel.setLayout(new GridLayout(1, 1));
		htmlPanel.add(htmlArea);
		mainGridBugPanel.add(htmlScrollPane, mainGridConstraints);
		
		htmlButton = new JButton("load html");
		mainGridConstraints.gridx = 1;
		mainGridConstraints.gridy = 0;
		mainGridConstraints.weightx = 0;
		mainGridConstraints.weighty = 0;
		mainGridBugPanel.add(htmlButton, mainGridConstraints);
		

		urlText = new JTextField();
		urlText.setMaximumSize(new Dimension(2000, 30));
		mainGridConstraints.gridx = 0;
		mainGridConstraints.gridy = 1;
		mainGridConstraints.weightx = 1;
		mainGridBugPanel.add(urlText, mainGridConstraints);

		goButton = new JButton("Go");
		mainGridConstraints.gridx = 1;
		mainGridConstraints.gridy = 1;
		mainGridConstraints.weightx = 0;
		mainGridBugPanel.add(goButton, mainGridConstraints);
		
		contentsText = new JTextField();
		contentsText.setMaximumSize(new Dimension(2000, 30));
		mainGridConstraints.gridx = 0;
		mainGridConstraints.gridy = 2;
		mainGridConstraints.weightx = 1;
		mainGridBugPanel.add(contentsText, mainGridConstraints);

		searchButton = new JButton("Search");
		mainGridConstraints.gridx = 1;
		mainGridConstraints.gridy = 2;
		mainGridConstraints.weightx = 0;
		mainGridBugPanel.add(searchButton, mainGridConstraints);
		
		
		scrollGridBugPanel = scrollGridBugPanel();
		JScrollPane scrollPane = setScrollPane(scrollGridBugPanel);
		mainGridConstraints.gridx = 0;
		mainGridConstraints.gridwidth = 2;
		mainGridConstraints.gridy = 3;
		mainGridConstraints.weightx = 1;
		mainGridConstraints.weighty = 1;
		mainGridBugPanel.add(scrollPane, mainGridConstraints);
		
		//dummky pane
		
		
		scrollGridConstraints = new GridBagConstraints();
		scrollGridConstraints.fill = GridBagConstraints.BOTH;
		
		
		frame = new JFrame();
		frame.setSize(new Dimension(400, 400));
		frame.setTitle("JsoupTextFinder");
		frame.setContentPane(mainGridBugPanel);
		frame.setVisible(true);
		
		//
		
		ImageIcon icon = new ImageIcon("jsoupFinder.png");
	    frame.setIconImage(icon.getImage());
	}
	
	public JPanel mainGridBugPanel(){
		JPanel jPanel = new JPanel();
		GridBagLayout gridLayout = new GridBagLayout();
		jPanel.setLayout(gridLayout);
		
		return jPanel;
	}
	
	public JScrollPane setScrollPane(JPanel jPanel){
		JScrollPane scrollPane = new JScrollPane(jPanel,
	            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
	            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setMinimumSize(new Dimension(100, 200));
		scrollPane.setSize(new Dimension(100, 200));
		return scrollPane;
	}
	
	public JPanel scrollGridBugPanel(){
		JPanel jPanel = new JPanel();
		GridBagLayout gridLayout = new GridBagLayout();
		jPanel.setLayout(gridLayout);
		return jPanel;
	}
	
	public void listener(){
		
		ActionListener htmlLoadListener = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String html = htmlArea.getText();
				htmlDocument = Jsoup.parse(html);
				
			}
		};
		htmlButton.addActionListener(htmlLoadListener);
		
		ActionListener urlLoadListener = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String url = urlText.getText();
				try {
					htmlDocument = Jsoup.connect(url).userAgent("Mozilla").get();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
		};
		
		urlText.addActionListener(urlLoadListener);
		goButton.addActionListener(urlLoadListener);
		
		ActionListener contentsSearchListener = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("search start");
				String contain = contentsText.getText();
				List<String> contents = JsoupControllerClass.searchContain(htmlDocument, contain);
				
				scrollGridBugPanel.removeAll();
				System.out.println("" + contents.size());
				for(int i = 0; i < contents.size(); i++) {
					scrollGridConstraints.gridx = 0;
					scrollGridConstraints.gridwidth = 1;
					scrollGridConstraints.gridy = 2 * i;
					scrollGridConstraints.weightx = 0;
					String content = contents.get(i);
					String btnString;
					if(content.length() < 40){
						btnString = content;
					}
					else{
						btnString = "..." + content.substring(content.length() - 35);
					}
					JButton selectaButton = new JButton(btnString);
					scrollGridBugPanel.add(selectaButton, scrollGridConstraints);
					selectaButton.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							Clipboard clipboard = Toolkit.getDefaultToolkit()
						            .getSystemClipboard();
						    StringSelection selection = new StringSelection(content);
						    clipboard.setContents(selection, selection);
						}
					});
					
					scrollGridConstraints.gridx = 0;
					scrollGridConstraints.gridwidth = 2;
					scrollGridConstraints.gridy = 2 * i + 1;
					scrollGridConstraints.weightx = 1;
					JLabel resultLabel = new JLabel();
					String result = JsoupControllerClass.select(htmlDocument, content);
					resultLabel.setText(result);
					
					
					scrollGridBugPanel.add(resultLabel, scrollGridConstraints);
					
					
					
				}
				Panel dummyPanel = new Panel();
				scrollGridConstraints.gridx = 0;
				scrollGridConstraints.gridy = contents.size() * 2;
				scrollGridConstraints.weightx = 1;
				scrollGridBugPanel.add(dummyPanel, scrollGridConstraints);

				scrollGridBugPanel.repaint();
				scrollGridBugPanel.revalidate();
			}
		};
		
		contentsText.addActionListener(contentsSearchListener);
		searchButton.addActionListener(contentsSearchListener);
		
		
	}
	
	
	private void hogeclick(){
		
		String contain = contentsText.getText();
		List<String> contents = JsoupControllerClass.searchContain(htmlDocument, contain);
		
		scrollGridBugPanel.removeAll();

		for(int i = 0; i < contents.size(); i++) {
			scrollGridConstraints.gridx = 0;
			scrollGridConstraints.gridy = i;
			scrollGridConstraints.weightx = 1;
			String content = contents.get(i);
			String btnString;
			if(content.length() < 15){
				btnString = content;
			}
			else{
				btnString = "..." + content.substring(content.length() - 10);
			}
			JButton selectaButton = new JButton(btnString);
			
			scrollGridBugPanel.add(selectaButton, scrollGridConstraints);
			
			scrollGridConstraints.gridx = 1;
			scrollGridConstraints.gridy = i;
			scrollGridConstraints.weightx = 0;
			JButton textButton = new JButton("test");
			scrollGridBugPanel.add(textButton, scrollGridConstraints);
			
			
		}
		

		scrollGridBugPanel.repaint();
		scrollGridBugPanel.revalidate();
	}
	
	
	
	
	
	
	
	
}
